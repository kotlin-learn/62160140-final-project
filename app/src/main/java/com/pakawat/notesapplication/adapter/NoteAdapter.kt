package com.pakawat.notesapplication.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pakawat.notesapplication.R
import com.pakawat.notesapplication.databinding.NoteItemBinding
import com.pakawat.notesapplication.entities.Note

class NoteAdapter(private val onItemClicked: (Note) -> Unit,
                  private val onItemLongClicked: (Note,View) -> Unit)
    : ListAdapter<Note, NoteAdapter.ItemViewHolder>(DiffCallback) {
    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Note>() {
            override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
                return oldItem == newItem
            }
        }
    }

    inner class ItemViewHolder(private var binding: NoteItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item:Note){
            binding.tvTitle.text = item.title
            binding.tvDesc.text = if (item.noteText.length>=90){
                "${item.noteText.substring(0,90)}..."
            }else{
                item.noteText
            }
            binding.tvDateTime.text = item.dateTime
            binding.imgPin.visibility  = if (item.isPin!!){
                 View.VISIBLE
            }else{
                View.GONE
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val viewHolder = ItemViewHolder(NoteItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
        viewHolder.itemView.setOnClickListener{
            val  position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val current  = getItem(position)
        holder.itemView.setOnClickListener{
            onItemClicked(current)
        }
        holder.itemView.setOnLongClickListener {
            //ShowPopup
            onItemLongClicked(current,holder.itemView)
            false
        }
        holder.bind(current)
    }


}