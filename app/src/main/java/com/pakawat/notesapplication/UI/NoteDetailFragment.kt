package com.pakawat.notesapplication.UI

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.pakawat.notesapplication.R
import com.pakawat.notesapplication.database.NoteDatabase
import com.pakawat.notesapplication.databinding.FragmentNoteDetailBinding
import com.pakawat.notesapplication.entities.Note
import com.pakawat.notesapplication.viewModel.NoteViewModel
import com.pakawat.notesapplication.viewModel.NoteViewModelFactory
import java.text.SimpleDateFormat
import java.util.*

class NoteDetailFragment : Fragment() {
//    variable
    private var _binding: FragmentNoteDetailBinding? = null
    private val binding get() = _binding!!
    private var isPin = false;
    private val viewModel : NoteViewModel by activityViewModels {
        NoteViewModelFactory(
            NoteDatabase.getDatabase(this.requireContext()).noteDao()
        )
    }
    lateinit var note:Note
    val TAG = "NoteDetailFragment"
    private val navigationArgs: NoteDetailFragmentArgs by navArgs()
//    end-variable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNoteDetailBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //retrieve from  home fragment
        val id = navigationArgs.noteId
        //Data binding
        viewModel.retrieveItem(id).observe(this.viewLifecycleOwner){
           item-> item.let {
            note = it
            binding.etNoteTitle.setText(it.title)
            binding.tvDateTime.setText(it.dateTime)
            binding.etNoteDesc.setText(it.noteText)
            isPin = it.isPin!!
            setPinColor()
           }
        }
        //onclick
        binding.apply {
            binding.imgBack.setOnClickListener {
                findNavController().navigate(R.id.action_noteDetailFragment_to_homeFragment)
            }
            binding.imgDone.setOnClickListener {
                //Update
                updateItem()
                findNavController().navigate(R.id.action_noteDetailFragment_to_homeFragment)

            }
            binding.imgDelete.setOnClickListener {
                //delete
                showConfirmationDialog()
            }
            binding.imgPin.setOnClickListener {
                if (isPin){
                    isPin=false
                    setPinColor()
                }else{
                    isPin=true
                    setPinColor()
                }
            }
            imgMore.setOnClickListener{
                showDialog()
            }
        }
    }

    private fun showDialog() {
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(R.layout.fragment_note_bottom)
        val imgDismiss = dialog.findViewById<ImageView>(R.id.imgMore)
        imgDismiss?.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun setPinColor() {
        if (isPin){ //true
            binding.imgPin.setColorFilter(Color.rgb(255,0,0))
        }else{
            binding.imgPin.setColorFilter(Color.rgb(255,255,255))
        }
    }

    private fun updateItem() {
        note.title = binding.etNoteTitle.text.toString()
        note.noteText = binding.etNoteDesc.text.toString()
        val sdf = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
        note.dateTime = sdf.format(Date())
        note.isPin = isPin
        viewModel.updateItem(note)
    }

    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.delete_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                deleteItem()
            }
            .show()
    }

    private fun deleteItem() {
        viewModel.deleteItem(note)
        findNavController().navigate(R.id.action_noteDetailFragment_to_homeFragment)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }
}