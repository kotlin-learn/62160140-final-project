package com.pakawat.notesapplication.UI

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.pakawat.notesapplication.R
import com.pakawat.notesapplication.adapter.NoteAdapter
import com.pakawat.notesapplication.database.NoteDatabase
import com.pakawat.notesapplication.databinding.FragmentHomeBinding
import com.pakawat.notesapplication.entities.Note
import com.pakawat.notesapplication.viewModel.NoteViewModel
import com.pakawat.notesapplication.viewModel.NoteViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment : Fragment() {
//    variable
    val TAG = "Home_fragment"
    private val viewModel : NoteViewModel by activityViewModels {
        NoteViewModelFactory(
            NoteDatabase.getDatabase(this.requireContext()).noteDao()
        )
    }
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    var notesArr = ArrayList<Note>()
//    end-variable

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            fabBtnCreateNote.setOnClickListener {
                findNavController().navigate(R.id.action_homeFragment_to_addNoteFragment)
            }
        }
        val adapter = setupAdapter()
        setupRecyclerView(adapter)
//      Data binding
        viewModel.allNotes.observe(this.viewLifecycleOwner) { notes ->
            notes.let { noteList->
                val newNotes : MutableList<Note> = mutableListOf()
                newNotes.addAll(noteList.filter {
                    it.isPin!!
                })
                newNotes.addAll(noteList.filter {
                    !it.isPin!!
                })
                adapter.submitList(newNotes)
                notesArr.clear()
                notesArr.addAll(newNotes)
            }

            GlobalScope.launch(Dispatchers.Main) {
                delay(200)
                binding.recyclerView.layoutManager?.scrollToPosition(0)
            }
        }
        //search func
        this.searchView(adapter)
    }



    private fun searchView(adapter:NoteAdapter) {
        binding.searchView.setOnQueryTextListener( object : SearchView.OnQueryTextListener{


            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {


                var tempArr = ArrayList<Note>()

                for (arr in notesArr){
                    if (arr.title.lowercase(Locale.getDefault()).contains(p0!!.lowercase())){
                        tempArr.add(arr)
                    }
                }

                adapter.submitList(tempArr)
                return true
            }

        })
    }



    private fun setupRecyclerView(
        adapter: NoteAdapter
    ) {
        binding.recyclerView.layoutManager = LinearLayoutManager(this.context)
        binding.recyclerView.adapter = adapter
    }

    private fun setupAdapter(): NoteAdapter {
        val adapter = NoteAdapter ({
            val action = HomeFragmentDirections
                .actionHomeFragmentToNoteDetailFragment(noteId = it.id)
            findNavController().navigate(action)
        },{
            selectedNote,itemView->
            val popupMenu = PopupMenu(activity, itemView)
            popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.pin -> {
                        viewModel.switchPin(selectedNote.id, pin = selectedNote.isPin!!)
                        Toast.makeText(activity,"Pin / Unpin Successful",Toast.LENGTH_SHORT).show()
                    }
                    R.id.delete -> {
                        viewModel.deleteItem(selectedNote)
                        Toast.makeText(activity,"Delete Successful",Toast.LENGTH_SHORT).show()

                    }
                }
                true
            }
            popupMenu.show()
        }
        )
        return adapter
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}