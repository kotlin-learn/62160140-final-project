package com.pakawat.notesapplication.UI

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.pakawat.notesapplication.R
import com.pakawat.notesapplication.database.NoteDatabase
import com.pakawat.notesapplication.databinding.FragmentAddNoteBinding
import com.pakawat.notesapplication.viewModel.NoteViewModel
import com.pakawat.notesapplication.viewModel.NoteViewModelFactory
import kotlinx.coroutines.flow.Flow
import java.text.SimpleDateFormat
import java.util.*



class AddNoteFragment : Fragment() {
//    variable
    private val viewModel : NoteViewModel by activityViewModels {
        NoteViewModelFactory(
            NoteDatabase.getDatabase(this.requireContext()).noteDao()
        )
    }
    private val TAG =  "Add_note_fragment"
    private var _binding: FragmentAddNoteBinding? = null
    private val binding get() = _binding!!
    val sdf = SimpleDateFormat("dd/M/yyyy HH:mm:ss")
//    end-variable


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddNoteBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            imgBack.setOnClickListener {
                findNavController().navigate(R.id.action_addNoteFragment_to_homeFragment)
            }
            imgDone.setOnClickListener {
                //Add -> Save -> go to HomeFragment
                //TODO: Save
                saveNote()
                //navigate
                findNavController().navigate(R.id.action_addNoteFragment_to_homeFragment)
            }
            imgMore.setOnClickListener{
                showDialog()
            }
            tvDateTime.text = sdf.format(Date())
        }
    }


    private fun showDialog() {
        val dialog = BottomSheetDialog(requireContext())
        dialog.setContentView(R.layout.fragment_note_bottom)
        val imgDismiss = dialog.findViewById<ImageView>(R.id.imgMore)
        imgDismiss?.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }



    private fun saveNote() {
        viewModel.addNewItem(
            binding.etNoteTitle.text.toString(),
            binding.tvDateTime.text.toString(),
            binding.etNoteDesc.text.toString(),
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}