package com.pakawat.notesapplication.viewModel

import androidx.lifecycle.*
import com.pakawat.notesapplication.dao.NoteDao
import com.pakawat.notesapplication.entities.Note
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat

class NoteViewModel (private val noteDao : NoteDao) : ViewModel(){
    val allNotes : LiveData<List<Note>> = noteDao.getAllNotes().asLiveData()

    //CRUD
    //TODO:C
    fun addNewItem(title:String,dateTime:String,noteText:String){
        val newItem = getNewItemEntry(title,dateTime,noteText)
        insertItem(newItem)
    }
    //decorate Note
    private fun getNewItemEntry(title:String,dateTime:String,noteText:String) : Note{

        return Note(title = title, dateTime = dateTime, noteText = noteText, isPin = false)
    }
    private fun insertItem(item: Note){
        viewModelScope.launch{
            noteDao.insert(item);
        };
    }
    //TODO:R
    fun retrieveItem(id:Int) : LiveData<Note>{
        return noteDao.getSpecificNote(id).asLiveData()
    }
    //TODO:U
     fun updateItem(item : Note){
        viewModelScope.launch {
            noteDao.update(item)
        }
    }
    //TODO:D
    fun deleteItem(item:Note){
        viewModelScope.launch {
            noteDao.delete(item)
        }
    }
    //END CRUD

    fun switchPin(id:Int,pin:Boolean){
        viewModelScope.launch {
            noteDao.pin(id,!pin)
        }
    }
}

//helper class
class NoteViewModelFactory(private val itemDao: NoteDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NoteViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return NoteViewModel(itemDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}