package com.pakawat.notesapplication.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

@Entity
data class Note(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name="title")
    var title: String,
    @ColumnInfo(name="date_time")
    var dateTime: String,
    @ColumnInfo(name = "note_text")
    var noteText:String,
    @ColumnInfo(name = "pin")
    var isPin:Boolean? = false
)
