package com.pakawat.notesapplication.dao

import androidx.room.*
import com.pakawat.notesapplication.entities.Note
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {
    @Query("SELECT * FROM note ORDER BY date_time DESC")
    fun getAllNotes() : Flow<List<Note>>

    @Query("SELECT * FROM note WHERE id =:id")
    fun getSpecificNote(id:Int) : Flow<Note>

    @Insert
    suspend fun insert(item:Note)

    @Delete
    suspend fun delete(item:Note)

    @Update
    suspend fun update(item:Note)

    @Query("UPDATE note SET pin = :pin WHERE id = :id")
    suspend fun pin(id:Int,pin:Boolean)
}